mod util;

use anyhow::Error;
use anyhow::Result;
use log::{debug, error, info, warn};

struct VulkanApp {}

impl VulkanApp {
    fn new() -> Result<Self, Box<Error>> {
        info!("Creating Application");

        Ok(Self {})
    }

    fn run(&mut self) {
        info!("Running Application")
    }
}

impl Drop for VulkanApp {
    fn drop(&mut self) {
        debug!("Dropping application.");
    }
}

fn main() -> Result<()> {
    env_logger::init();

    match VulkanApp::new() {
        Ok(mut app) => {
            app.run();
        }
        Err(error) => {
            error!("Failed to create application. Cause: {}", error)
        }
    }

    Ok(())
}
